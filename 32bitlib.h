        ; fnot - NOT a value, leave the result in place
	; pass the zero-page address of the value in .A

        ; fcopyzz - copy a series of bytes within zero page
        ; pass the zero-page address of the source in .Y
	; pass the zero-page address of the destination in .A
        ; pass the number of bytes in .X

        ; fcopymm - copy a series of bytes from one main memory location to another
        ; put the source address in COPYSRC
        ; put the destination address in COPYDEST
        ; pass the number of bytes in .X

        ; fcopymz - copy a series of bytes from main RAM to zero page
        ; source address is in COPYSRC
        ; destination address in zero page is in .X
        ; pass the number of bytes in .Y

        ; fcopyzm - copy a series of bytes from zero page to main RAM
        ; source address in zero page in in .X
        ; destination address in COPYDEST
        ; pass the number of bytes in .Y
        	
        ; feor - EOR two values, leave the result in the first one
        ; pass the zero-page address of the first value in .X
	; pass the zero-page address of the second value in .A

        ; fand - AND two values, leave the result in the first one
        ; pass the zero-page address of the first value in .X
	; pass the zero-page address of the second value in .A
        
	; shr_n - shift a 32-bit value to the right N times
        ; pass the number of times in .Y
        ; pass the zero-page address of the first byte in .X

       	; rotr_n - rotate a 32-bit value to the right N times
        ; pass the number of times in .Y
        ; pass the zero-page address of the first byte in .X

       	; rotl_n - rotate a 32-bit value to the left N times
        ; pass the number of times in .Y
        ; pass the zero-page address of the first byte in .X

               
	;; fmaj(a,b,c) = (a AND b) EOR (a AND c) EOR (b AND c)
               (3 AND 7) EOR (3 AND 12) EOR (7 AND 12)

               0000 0111
               0000 1100



               3 EOR 0 EOR 4 
