* Farm Life 128 - An RPG/Farm/Social/Dungeon/Fishing game for the C128
:LOGBOOK:
CLOCK: [2019-04-10 Wed 15:55]--[2019-04-10 Wed 18:19] =>  2:24
:END:
This project has been moved to its own repository: [[https://gitlab.com/aaron-baugher/farming-game-128][Farm Life 128]]

* Options
   #+FILETAGS: farm-game
   #+STARTUP: indent
