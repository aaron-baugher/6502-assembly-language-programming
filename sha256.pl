#!/usr/bin/env perl
use 5.010; use strict; use warnings;

my $rightrotate = sub {
    my $lo = $_[0] >> $_[1];
    my $hi = $_[0] << (32 - $_[1]);
    ($hi | $lo);
};

my $n = hex(shift @ARGV);
my $mask = 0xffffffff;

my($f1,$f2,$f3,$f4,$fch,$fmaj) = (0,0,0,0,0,0);

$f1 ^= $rightrotate->( $n, $_ ) for 2, 13, 22;
$f2 ^= $rightrotate->( $n, $_ ) for 6, 11, 25;
$f3 = $n >> 3;
$f3 ^= $rightrotate->( $n, $_ ) for 7, 18;
$f4 = $n >> 10;
$f4 ^= $rightrotate->( $n, $_ ) for 17, 19;


printf "f1: %0x\n", $f1 & $mask;
printf "f2: %0x\n", $f2 & $mask;
printf "f3: %0x\n", $f3 & $mask;
printf "f4: %0x\n", $f4 & $mask;

#$fch = ($e & $f) ^ ((~$e) & $g);
#$fmaj = ($a & $b) ^ ($a & $c) ^ ($b & $c);

 
