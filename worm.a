;;; worm - do a worm game like the one in bsdgames
;;; see at: aaron.baugher.biz
        
        !to "worm", cbm
        * = $1300

        !addr SCREEN    = $0400
        !addr BLINE     = $07c0
        !addr RANDOM    = $D41B
        !addr VICCURSOR = $0A27
	!addr BORDERCOLOR = $D020
        !addr BGCOLOR     = $D021
	!addr COLORRAM  = $d800
        
        !addr HEADADDR  = $FA   ; fa-fb address for head of worm
	!addr NUMADDR   = $FC   ; fc-fd address of random spot for number
	!addr TAILADDR  = $B0   ; b0-b1 address of the 'tail' of the worm
        !addr LENGTH    = $B2   ; b2-b3 length of worm
        !addr MOREBODY  = $B4   ; more body characters to add
        ;; list of pointers to worm body pieces $2000 - $27ff
        !addr HEADP     = $B6   ; pointer to the head location pointer
	!addr PRINTLOC = $B6
        !addr TAILP     = $B8   ; pointer to the tail location pointer
        !addr KEYPRESS  = $D4
	!addr LASTKEY   = $FE
        !addr JIFFIES   = $A2
        
        HEADCHAR  = $0F
        BODYCHAR  = $51
        EDGECHAR  = $2A
        SPACE     = $20
        BLACK     = $0
        WHITE     = $1
        CYAN      = $3
        
start:
	lda #0
        sta $ff00
	sta LASTKEY
        jsr setuprand
	lda #BLACK
        sta BORDERCOLOR
        sta BGCOLOR
	lda #WHITE
        jsr fillcolorram
	lda #$20
        jsr fillscreen
        lda #1
        sta VICCURSOR
        jsr drawborder
	lda #0
        sta HEADP
        sta TAILP
        lda #$20
        sta HEADP+1
        sta TAILP+1
        
	;; place head and tail at starting location $0600,
        ;; and other counters
startworm:
        lda #$F4
        ldy #0
        sta HEADADDR
	sta TAILADDR
        sta (HEADP),y
        lda #5
        sta HEADADDR+1
        sta TAILADDR+1
	iny
        sta (HEADP),y
        dey
        lda #HEADCHAR
        sta (HEADADDR),y
	lda #1
        sta LENGTH
        lda #0
        sta LENGTH+1
        lda #5
        sta MOREBODY
        jsr placeanum
        
mainloop:
        ;; wait for a keypress and do it
        jsr gamekeypress
	jsr printbody
        cmp #$1d                ; h - left
        bne ++
        ;; subtract 1 from HEADADDR
	lda HEADADDR
        bne +
        dec HEADADDR+1
+	dec HEADADDR
        jsr printhead
        jmp mainloop
++      cmp #$2a                ; l - right
        bne ++
        ;; add 1 to HEADADDR
        inc HEADADDR
        bne +
        inc HEADADDR+1
+       jsr printhead
        jmp mainloop
++      cmp #$25                ; k - up
        bne ++
        ;; subtract 40 from HEADADDR
        lda HEADADDR
        sec
        sbc #$28
        sta HEADADDR
	bcs +
        dec HEADADDR+1
+	jsr printhead
        jmp mainloop
++      cmp #$22                ; j - down
        bne ++
        ;; add 40 to HEADADDR
        lda HEADADDR
        clc
        adc #$28
        sta HEADADDR
	bcc +
	inc HEADADDR+1
+	jsr printhead
        jmp mainloop
++      cmp #$3e                ; q - quit
        beq +
        jmp mainloop

+       rts

        ;; check for collisions
collision:
	ldy #0
	lda (HEADADDR),y
        cmp #BODYCHAR
        bne +
        jmp endgame
+       cmp #EDGECHAR
        bne +
        jmp endgame
+       cmp #SPACE
        bne +
        rts
+       sec
        sbc #$30                ; get number value of number character
        sta MOREBODY
        jsr placeanum
        rts

        ;; end the game and print score?
endgame:
        ldy #0
-       lda endmsg1,y
	beq +
        sta $0572,y
        iny
        bne -
+       ldy #0
-       lda endmsg2,y
	beq +
        sta $059a,y
        iny
        bne -
+       ldy #0
-       lda endmsg1,y
	beq +
        sta $05c2,y
        iny
        bne -
+       ldy #0
-       lda endmsg3,y
	beq +
        sta $05ea,y
        iny
        bne -
+       ldy #0
-       lda endmsg1,y
	beq +
        sta $0612,y
        iny
        bne -
+       ldy #0
-       lda endmsg4,y
	beq +
        sta $063a,y
        iny
        bne -
+       ldy #0
-       lda endmsg1,y
	beq +
        sta $0662,y
        iny
        bne -
+	lda #5
        sta PRINTLOC+1
        lda #$f7
        sta PRINTLOC
        lda LENGTH
        sta dividend
        lda LENGTH+1
        sta dividend+1
        jsr print16bit
        jsr waitforyn
        brk
        
        ;; store A (the current key press), print new head, pull A
printhead:      
        pha
	jsr collision
        lda #HEADCHAR                 ; head character
        ldy #0
        sta (HEADADDR),y
	;; move HEADP up two bytes, wrap around if necessary
        inc HEADP
        inc HEADP
        bne +
        inc HEADP+1
        lda HEADP+1
        cmp #$28
        bne +
        lda #$20
        sta HEADP+1
	;; store HEADADDR where HEADP points
+       lda HEADADDR
        sta (HEADP),y
        iny
        lda HEADADDR+1
        sta (HEADP),y
	
        lda MOREBODY
        beq +
        dec MOREBODY
        inc LENGTH
        bne ++
        inc LENGTH+1
++      pla
	jsr delay1sec               
        rts
+       lda #SPACE
	ldy #0
        sta (TAILADDR),y
	;; move TAILP up two bytes, wrap around if necessary
        inc TAILP
        inc TAILP
        bne +
        inc TAILP+1
        lda TAILP+1
        cmp #$28
        bne +
        lda #$20
        sta TAILP+1
+       lda (TAILP),y
        sta TAILADDR
        iny
        lda (TAILP),y
        sta TAILADDR+1
        pla
	jsr delay1sec               
        rts


delay1sec:
        ldx #$80
        stx delay+1
--      stx delay
-       inc delay
        bne -
        inc delay+1
        bne --
        rts
        
                ;; store A (the current key press), print body character, pull A
printbody:      
        pha
        lda #BODYCHAR                 ; body character
        ldy #0
        sta (HEADADDR),y
        pla
        rts

        ;; place a number 1-9 in a space on screen
placeanum:      
	ldx RANDOM
        lda RANDOM              ; 10101010
        and #$03                ; 00000011 AND
        ora #$04                ; 00000100 OR 4
        cmp #$07
        bne +
        cpx #$E6                ; carry set if location is off screen
        bcc +
        jmp placeanum
+       stx NUMADDR
        sta NUMADDR+1
	ldy #0
        lda (NUMADDR),y
        cmp #SPACE
        bne placeanum
        lda RANDOM
        and #$07                ; 0-7 (needs to be digits 1-9!!)
        clc
        adc #$31                ; add '1' to it
        sta (NUMADDR),y
        rts

        ;; wait up to one second for a key press, put in .A
        ;; if time runs out, re-press last key
gamekeypress:     
	lda #$c4                ; 256-60 jiffies
        sta JIFFIES
-       lda KEYPRESS
        cmp #88
        bne +
        lda JIFFIES
        bne -
        lda LASTKEY
+	sta LASTKEY
        rts

waitforyn:     
-       lda KEYPRESS
        cmp #88
        beq -
	pha
-       lda KEYPRESS
        cmp #88
        bne -
	pla
        cmp #25                 ; pressed y
        bne +
        ;; y was pressed
	;; restore .SP to where it was when game started
        ldx #7
-       pla
        dex
        bne -
        jmp start
+       cmp #39                 ; pressed n
        bne waitforyn
	;; n was pressed, quit game
	lda #0
        sta $d0
        jmp $4000

        ;; draw a border of asterisks around the screen
drawborder:
	lda #$2A
        ldx #40
-       sta SCREEN-1,x
	sta BLINE-1,x
        dex
        bne -
	lda #$27
        sta HEADADDR
        lda #4
        sta HEADADDR+1
        ldx #24
-	lda #$2A
        ldy #0
        sta (HEADADDR),y
        iny
        sta (HEADADDR),y
	;; add 40 to HEADADDR pointer
        pha
        lda HEADADDR
        clc
        adc #40
        sta HEADADDR
        lda HEADADDR+1
        adc #0
        sta HEADADDR+1
        pla
        dex
        bne -
        rts

!src "randomlib.a"
!src "textscreenlib.a"        
!src "printnumbers.a"
        
delay:   !hex 00 00
endmsg1:  !scr "                 ", 0
endmsg2:  !scr "    game over!   ", 0
endmsg3:  !scr " your score:     ", 0
endmsg4:  !scr " play again? y/n ", 0

        
